package com.customization.test;

import com.customization.commons.WSDLUtil;
import net.sf.json.JSONObject;
import org.dom4j.*;
import org.junit.Test;

import java.util.Iterator;



public class TestWSDL extends BaseTest{

    //递归查询节点函数,输出节点名称
    private static void  getChildNodes(Element elem){
        System.out.println(elem.getName());
        Iterator<Node> it=    elem.nodeIterator();
        while (it.hasNext()){
            Node node = it.next();
            if (node instanceof Element){
                Element e1 = (Element)node;
                getChildNodes(e1);
            }

        }
    }

    @Test
    public void run() throws Exception {


        //接口地址，如果有多套测试环境 一般使用配置文件来配置address的域名部分，以便在不同环境下获得更好的支持
        String address="http://10.20.12.118/services/SrmOaService";
        String content="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.srm.weaver.com.cn\" xmlns:web1=\"http://webservice.srm.com\" xmlns:data=\"http://data.webservice.srm.com\"><soapenv:Header/><soapenv:Body><web:AwardOfBidApproving><web:in0><web1:WORKFLOW_TYPE><![CDATA[SRM40]]></web1:WORKFLOW_TYPE><web1:REQ_PERSON><![CDATA[10006731]]></web1:REQ_PERSON><web1:RFX_NUMBER><![CDATA[RFQ2020060100001]]></web1:RFX_NUMBER><web1:RFX_CATEGORY><![CDATA[询比价]]></web1:RFX_CATEGORY><web1:NOTIFIER><![CDATA[]]></web1:NOTIFIER><web1:COMPANY><![CDATA[安琪酵母股份有限公司]]></web1:COMPANY><web1:TITLE><![CDATA[SRM与hybris询价测试]]></web1:TITLE><web1:RFX_METHOD><![CDATA[邀请]]></web1:RFX_METHOD><web1:CURRENCY_CODE><![CDATA[CNY]]></web1:CURRENCY_CODE><web1:FEEDBACK_END_TIME><![CDATA[20200630]]></web1:FEEDBACK_END_TIME><web1:ITEM_CATEGORY><![CDATA[糖蜜、水解糖、葡萄糖母液]]></web1:ITEM_CATEGORY><web1:PROJECT_GROUP><![CDATA[国内物流采购项目组]]></web1:PROJECT_GROUP><web1:BUSINESS_DEPARTMENT><![CDATA[生产中心]]></web1:BUSINESS_DEPARTMENT><web1:RFX_ANNOUCEMENT_FLAG><![CDATA[Y]]></web1:RFX_ANNOUCEMENT_FLAG><web1:WIN_ANNOUCEMENT_FLAG><![CDATA[Y]]></web1:WIN_ANNOUCEMENT_FLAG><web1:ESTIMATED_TOTAL_PRICE><![CDATA[1000]]></web1:ESTIMATED_TOTAL_PRICE><web1:COMMENTS><![CDATA[中标]]></web1:COMMENTS><web1:BID_DOC_FEE><![CDATA[]]></web1:BID_DOC_FEE><web1:BID_DEPOSIT><![CDATA[]]></web1:BID_DEPOSIT><web1:BID_SCORING_METHOD><![CDATA[]]></web1:BID_SCORING_METHOD><web1:QUOTE_LINES_COUNTS><![CDATA[1]]></web1:QUOTE_LINES_COUNTS><web1:FEEDBACK_VENDOR_COUNTS><![CDATA[0]]></web1:FEEDBACK_VENDOR_COUNTS><web1:WIN_VENDOR_COUNTS><![CDATA[1]]></web1:WIN_VENDOR_COUNTS><web1:HYPER_LINK><![CDATA[&path=PUR7110&rfx_header_id=1067170&atm_counts=0&prequal_atm_counts=0&round=1&version=1]]></web1:HYPER_LINK><web1:LANGUAGE><![CDATA[ZHS]]></web1:LANGUAGE><web1:awardOfBidWLDetail><data:AwardOfBidWLDetail><data:INV_ORGANIZATION_CODE><![CDATA[P001]]></data:INV_ORGANIZATION_CODE><data:PUR_ORGANIZATION_CODE><![CDATA[1004]]></data:PUR_ORGANIZATION_CODE><data:ITEM_CATEGORY><![CDATA[项目设备]]></data:ITEM_CATEGORY><data:ITEM_CODE><![CDATA[11302795]]></data:ITEM_CODE><data:ITEM_DESC><![CDATA[G011001160007液位变送器]]></data:ITEM_DESC><data:UOM_CODE><![CDATA[PC]]></data:UOM_CODE><data:LINE_NUM><![CDATA[1]]></data:LINE_NUM><data:VENDOR_CODE><![CDATA[8107553]]></data:VENDOR_CODE><data:VENDOR_DESC><![CDATA[四川荣宏科技发展有限公司]]></data:VENDOR_DESC><data:LASTTIME_WIN_PRICE><![CDATA[1]]></data:LASTTIME_WIN_PRICE><data:ESTIMATED_UNIT_PRICE><![CDATA[]]></data:ESTIMATED_UNIT_PRICE><data:FB_NET_RETAIL_PRICE><![CDATA[300]]></data:FB_NET_RETAIL_PRICE><data:TAX_TYPE_CODE><![CDATA[13]]></data:TAX_TYPE_CODE><data:QUANTITY><![CDATA[1]]></data:QUANTITY><data:ALLOTTED_QUANTITY><![CDATA[1]]></data:ALLOTTED_QUANTITY><data:ALLOTTED_PROPORTION><![CDATA[100]]></data:ALLOTTED_PROPORTION><data:FEEDBACK_EXPIRY_DATE_FROM><![CDATA[20200601]]></data:FEEDBACK_EXPIRY_DATE_FROM><data:FEEDBACK_EXPIRY_DATE_TO><![CDATA[20200630]]></data:FEEDBACK_EXPIRY_DATE_TO><data:NEED_BY_DATE><![CDATA[]]></data:NEED_BY_DATE></data:AwardOfBidWLDetail></web1:awardOfBidWLDetail><web1:awardOfBidGYSDetail><data:AwardOfBidGYSDetail><data:LINE_NUM><![CDATA[1]]></data:LINE_NUM><data:VENDOR_CODE><![CDATA[8107553]]></data:VENDOR_CODE><data:VENDOR_DESC><![CDATA[四川荣宏科技发展有限公司]]></data:VENDOR_DESC><data:VENDOR_WIN_AMOUNT><![CDATA[300]]></data:VENDOR_WIN_AMOUNT></data:AwardOfBidGYSDetail></web1:awardOfBidGYSDetail></web:in0></web:AwardOfBidApproving></soapenv:Body></soapenv:Envelope>";
        JSONObject res=new WSDLUtil().HttpClientPost(address,content);
        System.out.println("接口耗时:"+res.getString("time"));
        System.out.println("接口返回值:"+res.getString("result").toString());

        try {
            //todo 这部分解析可以考虑使用 jdom的 xpath来进行处理

            Document document = DocumentHelper.parseText(res.getString("result").toString());
            //获得更深层次的标签（一层一层的获取）
            Element ele = document.getRootElement().element("Body").element("AwardOfBidApprovingResponse").element("out");
            Element OA_FLOW=ele.element("OA_FLOW");
            Element RESPONSE_MESSAGE=ele.element("RESPONSE_MESSAGE");
            Element RESPONSE_STATUS=ele.element("RESPONSE_STATUS");
            System.out.println("解析之后的返回值:");
            System.out.println(OA_FLOW.getName()+"："+OA_FLOW.getTextTrim());
            System.out.println(RESPONSE_MESSAGE.getName()+"："+RESPONSE_MESSAGE.getTextTrim());
            System.out.println(RESPONSE_STATUS.getName()+"："+RESPONSE_STATUS.getTextTrim());

        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }
}
