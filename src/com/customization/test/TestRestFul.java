package com.customization.test;

import com.alibaba.fastjson.JSONObject;
import com.cloudstore.dev.api.util.HttpManager;
import com.cloudstore.dev.api.util.Util_Security;

import weaver.rsa.security.RSA;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class TestRestFul {

    public static void main(String[] args) {
        TestRestFul   testRestFul=new TestRestFul();


        //注册信息
        JSONObject registInfo=testRestFul.regist();
        System.out.println(registInfo.toString());
        String secrit=registInfo.getString("secrit");
        String spk=registInfo.getString("spk");

        //获取token
        JSONObject tokenInfo=testRestFul.applytoken(secrit,spk);
        String token=tokenInfo.getString("token");

        //调用接口
        testRestFul.testApi(token,"1",spk);

    }


    public JSONObject regist() {
        //httpclient的使用请自己封装，可参考ECOLOGY中HttpManager类
        HttpManager http = new HttpManager();
        //请求头信息封装集合
        Map<String, String> heads = new HashMap<String, String>();
        //获取当前异构系统RSA加密的公钥
        String cpk = new RSA().getRSA_PUB();
        //当前异构系统用于向ECOLOGY注册时使用的账号密码通过DES加密后密文进行传输
        //kb1906及以上版本 已废弃账号密码校验
        String password = Util_Security.encoded_des("1");
        //封装参数到请求头
        heads.put("appid", "EEAA5436-7577-4BE0-8C6C-89E9D88805EA");
        heads.put("cpk", cpk);
        //kb1906及以上版本 已废弃账号密码校验
        heads.put("loginid", "sysadmin");
        //kb1906及以上版本 已废弃账号密码校验
        heads.put("pwd", password);
        //调用ECOLOGY系统接口进行注册
        String data = null;
        try {
            data = http.postDataSSL("http://129.211.19.119/api/ec/dev/auth/regist", new HashMap<>(), heads);
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        //返回的数据格式为json，具体格式参数格式请参考文末API介绍。
        //注意此时如果注册成功会返回秘钥信息,请根据业务需要进行保存。
        JSONObject json=JSONObject.parseObject(data);


        return json;
    }

    /**
     *
     * @param secret 调用注册接口返回的secret参数值
     * @param spk 注册接口成功时返回的apk参数值
     * @throws Exception
     * @return
     */
    public JSONObject applytoken(String secret, String spk){
        //httpclient的使用请自己封装，可参考ECOLOGY中HttpManager类
        HttpManager http = new HttpManager();
        //请求头信息封装集合
        Map<String, String> heads = new HashMap<String, String>();
        //ECOLOGY返回的系统公钥

        RSA rsa = new RSA();
        //对秘钥进行加密传输，防止篡改数据
         secret = rsa.encrypt(null, secret, null, "utf-8", spk, false);
        //封装参数到请求头
        heads.put("appid", "EEAA5436-7577-4BE0-8C6C-89E9D88805EA");
        heads.put("secret", secret);
        //调用ECOLOGY系统接口进行申请
        String data = null;
        try {
            data = http.postDataSSL("http://129.211.19.119/api/ec/dev/auth/applytoken", new HashMap<>(), heads);
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        //返回的数据格式为json，具体格式参数格式请参考文末API介绍。
        //注意此时如果申请成功会返回token,请根据业务需要进行保存。
        System.out.println("applytoken:"+data);
        return JSONObject.parseObject(data);
        /**********************其它业务逻辑实现***********************/
    }


    /**
     *
     * @param token 申请token接口成功时返回token值
     * @param userid 真实的ECOLOGY用户ID
     * @param spk 注册接口成功时返回的apk参数值
     */
    public  void testApi(String token,String userid,String spk) {
        //httpclient的使用请自己封装，可参考ECOLOGY中HttpManager类
        HttpManager http=new HttpManager();
        //请求头信息封装集合
        Map<String, String> heads=new HashMap<String, String>();
        //接口参数信息封装集合
        Map<String, String> param=new HashMap<String, String>();

        //ECOLOGY返回的系统公钥

        RSA rsa = new RSA();
        //对用户真实系统id进行加密传输，防止篡改数据
         userid = rsa.encrypt(null, userid, null, "utf-8", spk, false);
        //封装参数到请求头
        heads.put("token", token);
        heads.put("appid", "EEAA5436-7577-4BE0-8C6C-89E9D88805EA");
        heads.put("userid", userid);
        //......
        //封装接口请求参数
        //......
        //调用接口相关系统接口
        String data= null;
        try {
            data = http.postDataSSL("http://129.211.19.119/api/workflow/paService/getDoingWorkflowRequestList", param,heads);
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        //返回接口响应参数
        System.out.println("待办列表："+data);




        //提交流程
        param.put("requestId","207207");
        param.put("remark","我是通过restful提交上来的签字意见");
        try {
            data = http.postDataSSL("http://129.211.19.119/api/workflow/paService/submitRequest", param,heads);
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        //返回接口响应参数
        System.out.println("提交结果："+data);




        /**********************其它业务逻辑实现***********************/
    }
}
