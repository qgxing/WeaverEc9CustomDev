package com.customization.test;

import com.api.browser.web.BrowserAction;
import com.customization.api.pdf2word.BaiduDemoApplication;
import com.customization.api.pdf2word.DemoApplication;
import com.customization.commons.LocalTestAction;
import com.engine.workflow.web.RequestFormAction;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName TestAction.java
 * @Description TODO
 * @createTime 2020-05-15 10:55:00
 */
public class TestAction  extends BaseTest {

    /**
     * 测试流程接口
     * @throws Exception
     */
    @Test
    public void testMM050_node688_after() throws Exception {
        LocalTestAction test = new LocalTestAction(2116237,
                "com.customization.action.demo.MM050_node688_after");
        test.setLastOperator(63);//操作人
        test.setRemark("系统管理员本地测试提交流程");
        test.setSrc("submit");//提交方式 不设置默认值:submit
        test.setSumitNext(false);//流程是否提交到下一步 默认值 false
        Assert.assertEquals(test.execute(), "1");
    }


    /**
     * 测试流程接口
     * @throws Exception
     */
    @Test
    public void testAngelHybrisIT14Action() throws Exception {
        LocalTestAction test = new LocalTestAction(2242342, "weaver.angel.action.AngelHybrisIT14Action");
        test.setLastOperator(236);
        test.setSrc("submit");
        test.setRemark("系统管理员本地测试提交流程");
        Assert.assertEquals(test.execute(), "1");
    }

    /**
     * 测试FI15
     * @throws Exception
     */
    @Test
    public void testAngelSapBorrowPaymentAction() throws Exception {
        
        LocalTestAction test = new LocalTestAction(2270394, "weaver.angel.action.AngelSapBorrowPaymentAction");
        test.setLastOperator(80);
        test.setSrc("submit");
        test.setRemark("系统管理员本地测试提交流程");
        Assert.assertEquals(test.execute(), "1");
    }


    /**
     * 测试FI15
     * @throws Exception
     */
    @Test
    public void testAngelSRM_PM03() throws Exception {

        LocalTestAction test = new LocalTestAction(2265368, "weaver.angel.action.srm.AngelSRM_PM03");
        test.setLastOperator(6676);
        test.setSrc("submit");
        test.setRemark("系统管理员本地测试提交流程");
        Assert.assertEquals(test.execute(), "1");
    }


    @Test
    public void testPdf2Txt() throws Exception {
        String path = "/Users/liutaihong/Downloads/收文登记/关于印发《湖北省生产单元复工复产工作大纲》的通知.pdf";

        new BaiduDemoApplication().pdfParse(path);

        path = "/Users/liutaihong/Downloads/收文登记/收文登记02.pdf";
       // new BaiduDemoApplication().pdfParse(path);

        //BrowserAction
       // com.api.browser.service.impl.ResourceBrowserService

    }

    public static void main(String[] args) {
        String data="{\"datas\":[{\"subcompanyid1span\":\"宜昌市政府\",\"subcompanyid\":\"5\",\"pinyinlastname\":\"李妍pinyinlastname\",\"departmentidspan\":\"市领导\",\"departmentid\":\"2\",\"icon\":\"/messager/images/icon_w_wev8.jpg\",\"subcompanyname\":\"宜昌市政府\",\"type\":\"resource\",\"supsubcompanyname\":\"\",\"title\":\"李妍|副总经理|宜昌市政府|市领导\",\"lastname\":\"李妍lastname\",\"jobtitlespan\":\"副总经理\",\"jobtitlename\":\"副总经理\",\"departmentname\":\"市领导\",\"lastnamespan\":\"李妍lastnamespan\",\"name\":\"李妍name\",\"id\":\"4\",\"displayKeys\":[\"lastnamespan\",\"jobtitlespan\"],\"nodeid\":\"resource_4x\",\"supsubcompanyid\":\"0\"}]}";
        System.out.println(data);
        JSONObject json = JSONObject.fromObject(data);
        if(json.containsKey("datas")){
            JSONArray jsonArray=json.getJSONArray("datas");
            for (int i = 0; i <jsonArray.size() ; i++) {
                json.getJSONArray("datas").getJSONObject(i).put("lastname",
                        json.getJSONArray("datas").getJSONObject(i).get("title"));
            }
        }
        System.out.println(json);



    }

}
