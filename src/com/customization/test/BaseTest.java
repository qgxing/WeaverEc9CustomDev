package com.customization.test;

import com.customization.commons.Console;
import org.junit.Before;
import weaver.conn.RecordSet;
import weaver.general.GCONST;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class BaseTest {

	@Before
	public void before() throws Exception {
		GCONST.setServerName("ecology");
		//GCONST.setRootPath("D:\\devIdeaProjects\\WeaverEc9CustomDev\\web\\"); //window操作系统写法
		//GCONST.setRootPath("/Users/liutaihong/IdeaProjects/WeaverEc9CustomDev/web/");//MacOS 操作系统写法
		GCONST.setRootPath("/Users/liutaihong/IdeaProjects/WeaverEc9CustomDev/web/");


		String hostname = "Unknown";
		try
		{
			InetAddress addr= InetAddress.getLocalHost();
			hostname = addr.getHostName();
		}
		catch (UnknownHostException ex)
		{
			System.out.println("Hostname can not be resolved");
		}
		Console.log( hostname);
	}


}
