package com.customization.cronjob;

import com.customization.commons.Console;
import weaver.conn.RecordSet;
import weaver.interfaces.schedule.BaseCronJob;

import java.util.UUID;

/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName SynToDoSomething.java
 * @Description TODO
 * @createTime 2020-06-02 8:45:00
 */
public class SynToDoSomething extends BaseCronJob {

    @Override
    public void execute() {
        Console.log("CronJob SynToDoSomething execute start");
        this.runSomeThing();
        Console.log("CronJob SynToDoSomething execute end");

    }

    /**
     * 计划任务实际执行的方法体
     */
    public void runSomeThing(){

        //todo  在这里写我们想做的任何事情
        for (int i = 0; i <6000 ; i++) {

            try {
                Thread.sleep(1000);
                Console.log("计划任务 我在执行我想做的事情,这是循环的次数"+i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            RecordSet recordSet = new RecordSet();
            recordSet.execute("update syncronjob set status='1',datestr='"+System.currentTimeMillis()+"' where syntype='SendNewsJob'");//标记为已经有线程在跑

        }


    }


    /**
     * 通过监控是否有正在执行的计划任务来确保在上一次计划任务未执行完成前，再次触发改计划任务
     */
    private void executeDemo(){

        String uuid= UUID.randomUUID().toString().replace("-","");
        Console.log(" execute BaseCronJob SendNewsJob ================START================= "+uuid);

        long startreadSap = System.currentTimeMillis();

        RecordSet recordSet = new RecordSet();
        recordSet.execute("select * from syncronjob  where syntype='SendNewsJob'");//记录当前是否已经有线程在跑
        boolean syncProcess = false;
        if (recordSet.next()) {
            if (recordSet.getInt("status") == 0) {
                syncProcess = true;
            } else {

                long lastreadSapTime = Long.parseLong(recordSet.getString("datestr"));
                long nowTime = System.currentTimeMillis();
                Console.log("lastreadSapTime：" + lastreadSapTime);
                Console.log("nowTime：" + nowTime);
                Console.log("距离上次同步时间：" + (nowTime - lastreadSapTime) / 1000 / 60);
                if ((nowTime - lastreadSapTime) / 1000 / 60 > 5) {//增加距离最后一次同步时间大于5分的重新可继续执行计划任务
                    syncProcess = true;
                } else {
                    Console.log("当前正在同步中.....  update syncronjob set status='0' ");
                    Console.log("距离上次开始任务没有超过5分钟，请等待上次任务执行完成");
                    Console.log("================END=================");
                }

            }
        } else {
            recordSet.execute("insert  into  syncronjob (status,datestr,syntype) values ('0','" + System.currentTimeMillis() + "','SendNewsJob')");
            syncProcess = true;
        }
        if (syncProcess) {
            recordSet.execute("update syncronjob set status='1',datestr='"+System.currentTimeMillis()+"' where syntype='SendNewsJob'");//标记为已经有线程在跑
            this.runSomeThing();
            recordSet.execute("update syncronjob set status='0' where syntype='SendNewsJob'");//标记当前同步进程已经结束
            Console.log("整体耗时：" + (System.currentTimeMillis() - startreadSap) / 1000 + "s");
            Console.log("================END=================");
        }
        Console.log(" execute BaseCronJob SendNewsJob ================END================= "+uuid);

    }

}
