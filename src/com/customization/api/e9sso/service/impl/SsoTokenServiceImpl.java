package com.customization.api.e9sso.service.impl;

import com.customization.api.e9sso.cmd.*;
import com.customization.api.e9sso.service.SsoTokenService;

import com.engine.core.impl.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class SsoTokenServiceImpl extends Service implements SsoTokenService {

    @Override
    public Map<String, Object> gettokenInfo(Map<String, Object> params) {
        return   commandExecutor.execute(new SsoTokenCmd(user,params));
    }

    @Override
    public Map<String, Object> sendMsg(Map<String, Object> params) {
        return commandExecutor.execute(new SendMsgCmd(user,params));
    }

    @Override
    public Map<String, Object> updateStatus4Bz(Map<String, Object> params) {
        return commandExecutor.execute(new UpdateStatus4BzCmd(user,params));
    }

    @Override
    public Map<String, Object> dispatch(Map<String, Object> params) {
        return commandExecutor.execute(new DispatchSSFCmd(user,params));
    }


    @Override
    public Map<String, Object> reject(HttpServletRequest request) {
        return commandExecutor.execute(new RejectCmd(user, request));
    }



}
