package com.customization.api.e9sso.service;

import cpcns.http.HttpRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface SsoTokenService {

    Map<String, Object> gettokenInfo(Map<String, Object> params);


    Map<String, Object> sendMsg(Map<String, Object> params);

    Map<String, Object> updateStatus4Bz(Map<String, Object> params);

    Map<String, Object> dispatch(Map<String, Object> params);


    Map<String, Object> reject(HttpServletRequest request);


}
