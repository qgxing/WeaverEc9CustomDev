package com.customization.api.e9sso.web;

import com.alibaba.fastjson.JSONObject;
import com.customization.api.e9sso.service.SsoTokenService;
import com.customization.api.e9sso.service.impl.SsoTokenServiceImpl;
import com.engine.common.util.ParamUtil;
import com.engine.common.util.ServiceUtil;
import weaver.hrm.HrmUserVarify;
import weaver.hrm.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

public class SsoTokenAction {

    private SsoTokenService getService(User user) {
        return ServiceUtil.getService(SsoTokenServiceImpl.class, user);
    }


    @GET
    @Path("/token")
    @Produces({MediaType.TEXT_PLAIN})
    public String getToken(@Context HttpServletRequest request, @Context HttpServletResponse response){
        Map<String, Object> apidatas = new HashMap<String, Object>();
        try {
            //获取当前用户
            User user = HrmUserVarify.getUser(request, response);
            apidatas.putAll(getService(user).gettokenInfo(ParamUtil.request2Map(request)));

        } catch (Exception e) {
            e.printStackTrace();
            apidatas.put("status", false);
            apidatas.put("api_errormsg", "catch exception : " + e.getMessage());
        }
        return JSONObject.toJSONString(apidatas);
    }

    @POST
    @Path("/sendMsg")
    @Produces({MediaType.TEXT_PLAIN})
    public String sendMsg(@Context HttpServletRequest request, @Context HttpServletResponse response){
            Map<String, Object> apidatas = new HashMap<String, Object>();
            try {
                //获取当前用户
                User user = HrmUserVarify.getUser(request, response);
                apidatas.putAll(getService(user).sendMsg(ParamUtil.request2Map(request)));

            } catch (Exception e) {
                e.printStackTrace();
                apidatas.put("status", false);
                apidatas.put("api_errormsg", "catch exception : " + e.getMessage());
            }
            return JSONObject.toJSONString(apidatas);
    }

    @POST
    @Path("/updateStatus4Bz")
    @Produces({MediaType.TEXT_PLAIN})
    public String updateStatus4Bz(@Context HttpServletRequest request, @Context HttpServletResponse response){
        Map<String, Object> apidatas = new HashMap<String, Object>();
        try {
            //获取当前用户
            User user = HrmUserVarify.getUser(request, response);
            apidatas.putAll(getService(user).updateStatus4Bz(ParamUtil.request2Map(request)));

        } catch (Exception e) {
            e.printStackTrace();
            apidatas.put("status", false);
            apidatas.put("api_errormsg", "catch exception : " + e.getMessage());
        }
        return JSONObject.toJSONString(apidatas);
    }

    @POST
    @Path("/dispatch")
    @Produces({MediaType.TEXT_PLAIN})
    public String dispatch(@Context HttpServletRequest request, @Context HttpServletResponse response){
        Map<String, Object> apidatas = new HashMap<String, Object>();
        try {
            //获取当前用户
            User user = HrmUserVarify.getUser(request, response);
            apidatas.putAll(getService(user).dispatch(ParamUtil.request2Map(request)));

        } catch (Exception e) {
            e.printStackTrace();
            apidatas.put("status", false);
            apidatas.put("api_errormsg", "catch exception : " + e.getMessage());
        }
        return JSONObject.toJSONString(apidatas);
    }


    @POST
    @Path("/reject")
    @Produces({MediaType.TEXT_PLAIN})
    public String reject(@Context HttpServletRequest request, @Context HttpServletResponse response){
        Map<String, Object> apidatas = new HashMap<String, Object>();
        try {
            //获取当前用户
            User user = HrmUserVarify.getUser(request, response);
            apidatas.putAll(getService(user).reject(request));

        } catch (Exception e) {
            e.printStackTrace();
            apidatas.put("status", false);
            apidatas.put("api_errormsg", "catch exception : " + e.getMessage());
        }
        return JSONObject.toJSONString(apidatas);
    }

    public static void main(String[] args) {
        String  DOC_TYPE="";//流程类型，取流程前面编号
        String  DOC_TYPE_D="FI201、sdjdd";
       
        System.out.println("DOC_TYPE:"+DOC_TYPE);
    }



}
