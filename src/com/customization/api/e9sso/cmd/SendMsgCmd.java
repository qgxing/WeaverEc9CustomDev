package com.customization.api.e9sso.cmd;

import com.cloudstore.dev.api.bean.MessageBean;
import com.cloudstore.dev.api.bean.MessageType;
import com.cloudstore.dev.api.util.Util_Message;
import com.engine.common.biz.AbstractCommonCommand;
import com.engine.common.entity.BizLogContext;
import com.engine.core.interceptor.CommandContext;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.hrm.User;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SendMsgCmd   extends AbstractCommonCommand<Map<String,Object>> {


    public SendMsgCmd(User user, Map<String,Object> params) {
        this.user = user;
        this.params = params;


    }

    @Override
    public BizLogContext getLogContext() {
        return null;
    }

    @Override
    public Map<String, Object> execute(CommandContext commandContext) {
        Map<String, Object> apidatas = new HashMap<String, Object>();
        apidatas.put("status",true);
        apidatas.put("msg","发送成功");

        RecordSet rs=new RecordSet();



        int typeid=Util.getIntValue(Util.null2String(params.get("typeid")),-1);

        String title=Util.null2String(params.get("title"));
        String context=Util.null2String(params.get("context"));
        String linkUrl=Util.null2String(params.get("linkUrl"));
        String linkMobileUrl=Util.null2String(params.get("linkMobileUrl"));
        String receiver=Util.null2String(params.get("receiver"));//接收人编码 多个之间用逗号隔开

        if(typeid==-1||context.equals("")||title.equals("")||receiver.equals("")){
            apidatas.put("status",false);
            apidatas.put("msg","必填参数不允许为空");
            return apidatas;
        }


        Set<String> userIdList=new HashSet<String>();



        int creator=1;
        rs.execute("select *  from hrmresource where loginid='bz'");
        if(rs.next()){
            creator=rs.getInt("id");
        }

        String [] res=receiver.split(",");
        String  receiverStr="'0'";
        for (String s:  res) {
            receiverStr+=",'"+s+"'";
        }

        rs.execute("select * from hrmresource where workcode in("+receiverStr+")");
        if(rs.next()){
            userIdList.add(rs.getString("id"));
        }
        boolean resb=pushMsg( typeid, title,  context,  linkUrl,  linkMobileUrl,  userIdList, creator);

        if(!resb){
            apidatas.put("status",false);
            apidatas.put("msg","发送失败");
        }
        return apidatas;
    }


    /**
     *
     * @param typeid 消息来源ID
     * @param title 标题
     * @param context 内容
     * @param linkUrl PC端链接 纯文本就传空字符串
     * @param linkMobileUrl 移动端链接 纯文本就传空字符串
     * @param userIdList     Set<String> 人员ID
     * @param creator 创建人ID
     * @return
     */
    private boolean pushMsg(int typeid,String title, String context, String linkUrl, String linkMobileUrl, Set<String> userIdList,int creator) {
        MessageType messageType = MessageType.newInstance(typeid); // 消息来源（见文档第四点补充）
        try {
            MessageBean messageBean = Util_Message.createMessage(messageType, userIdList, title, context, linkUrl,
                    linkMobileUrl);
            messageBean.setCreater(creator);// 创建人id
            return Util_Message.store(messageBean);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}
