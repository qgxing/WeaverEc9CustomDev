package com.customization.api.e9sso.cmd;

import com.customization.commons.Console;
import com.engine.common.biz.AbstractCommonCommand;
import com.engine.common.entity.BizLogContext;
import com.engine.core.interceptor.CommandContext;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.hrm.User;

import java.util.HashMap;
import java.util.Map;

public class UpdateStatus4BzCmd extends AbstractCommonCommand<Map<String,Object>> {


    public UpdateStatus4BzCmd(User user, Map<String,Object> params) {
        this.user = user;
        this.params = params;


    }

    @Override
    public BizLogContext getLogContext() {
        return null;
    }

    @Override
    public Map<String, Object> execute(CommandContext commandContext) {
        Map<String, Object> apidatas = new HashMap<String, Object>();
        apidatas.put("status",true);
        apidatas.put("msg","更新成功!");

        RecordSet rs=new RecordSet();

        String requestid=Util.null2String(params.get("requestid"));
        String num=Util.null2String(params.get("num"));

        if("".equals(requestid)){
            apidatas.put("status",false);
            apidatas.put("msg","requestid不能为空或者不存在");
            return apidatas;
        }



        String sql = "SELECT T1.REQUESTID, T1.WORKFLOWID, T1.REQUESTNAME,T2.ISBILL, "
                + " T1.CREATER, T2.FORMID FROM WORKFLOW_REQUESTBASE T1"
                + " INNER JOIN WORKFLOW_BASE T2 ON T2.ID=T1.WORKFLOWID"
                + " WHERE T1.REQUESTID=?";

        String tablename="";
        if (rs.executeQuery(sql, requestid) && rs.next()) {
            tablename="formtable_main_"+Math.abs(rs.getInt("FORMID"));
        }

        if("".equals(tablename)){
            apidatas.put("status",false);
            apidatas.put("msg","requestid无效或者不存在!");
            return apidatas;
        }

        sql="select * from "+tablename+" where  requestid='"+requestid+"'";
        if(rs.execute(sql)&&rs.next()){
            Console.log(requestid+"更新之前的值:"+rs.getString("bzxtlsh"));
            Console.log(requestid+"更新之后的值:"+num);
        }

        sql=" update "+tablename+"  set bzxtlsh='"+num+"'  where requestid='"+requestid+"'";
        if(!rs.execute(sql)){
            apidatas.put("status",false);
            apidatas.put("msg","更新失败!");
        }

        return apidatas;
    }
}
