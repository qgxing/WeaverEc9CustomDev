package com.customization.api.e9sso.cmd;

import com.customization.commons.Console;
import com.engine.common.biz.AbstractCommonCommand;
import com.engine.common.entity.BizLogContext;
import com.engine.core.interceptor.CommandContext;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.hrm.User;
import weaver.workflow.webservices.WorkflowServiceImpl;

import java.util.HashMap;
import java.util.Map;

public class DispatchSSFCmd extends AbstractCommonCommand<Map<String,Object>> {


    private String appid;
    public DispatchSSFCmd(User user, Map<String,Object> params) {
        this.user = user;
        this.params = params;
        appid=Util.null2String(params.get("appid"));

    }

    @Override
    public BizLogContext getLogContext() {
        return null;
    }

    @Override
    public Map<String, Object> execute(CommandContext commandContext) {


        Map<String, Object> apidatas = new HashMap<String, Object>();
        apidatas.put("status",true);
        apidatas.put("msg","派单成功!");

        RecordSet rs=new RecordSet();



        String requestid=Util.null2String(params.get("requestId"));
        String dispatchWorkcode=Util.null2String(params.get("dispatchWorkcode"));
        if(requestid.equals("")||dispatchWorkcode.equals("")){
            apidatas.put("status",false);
            apidatas.put("msg","必要参数不能为空或者不存在");
            return apidatas;
        }
        Console.log(this.appid+"中的'"+user.getUID()+"'把'"+requestid+"'派给"+dispatchWorkcode);
        boolean b=rs.execute("insert into uf_DispatchSSF (request_id,oper_workcode) values('"+requestid+"','"+dispatchWorkcode+"')");
        if(!b) {
            apidatas.put("status",b);
            apidatas.put("msg", "派单失败!");
            return apidatas;
        }
        String type = "submit";//submit 提交 subnoback提交不需返回  subback提交需要返回 reject
        int userid = user.getUID();
        int reqid=Integer.parseInt(requestid);
        String remark = "SSF派单后自动给收单人";
        weaver.workflow.webservices.WorkflowRequestInfo requestInfo = new WorkflowServiceImpl().getWorkflowRequest(reqid, userid, reqid);
        String resp = new WorkflowServiceImpl().submitWorkflowRequest(requestInfo, reqid, userid, type, remark);
        if (!"success".equals(resp)) {
            apidatas.put("msg", "派单后给流程授权失败");
            apidatas.put("status",false);
        }
        return apidatas;
    }
}
