package com.customization.api.pdf2word.service.impl;

import com.customization.api.pdf2word.cmd.CheckWfPdfStatusCmd;
import com.customization.api.pdf2word.cmd.CollectWordsCmd;
import com.customization.api.pdf2word.service.PDF2WordService;

import com.engine.core.impl.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public class PDF2WordServiceImpl  extends Service implements PDF2WordService {


    @Override
    public Map<String, Object> checkWfPdfStatus(HttpServletRequest request, HttpServletResponse response) {
        return commandExecutor.execute(new CheckWfPdfStatusCmd(request,response));
    }

    @Override
    public Map<String, Object> collectWords(HttpServletRequest request, HttpServletResponse response) {
        return commandExecutor.execute(new CollectWordsCmd(request,response));
    }

}
