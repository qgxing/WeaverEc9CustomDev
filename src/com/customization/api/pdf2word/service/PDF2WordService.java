package com.customization.api.pdf2word.service;

import com.qiyuesuo.sdk.v2.bean.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface PDF2WordService {

    /**
     * 检查是否配置了PDF识别的流程
     * @return
     */
    Map<String, Object> checkWfPdfStatus(HttpServletRequest request, HttpServletResponse response);

    /**
     * 提取PDF关键字
     * @param request
     * @param response
     * @return
     */
    Map<String, Object> collectWords(HttpServletRequest request, HttpServletResponse response);
}
