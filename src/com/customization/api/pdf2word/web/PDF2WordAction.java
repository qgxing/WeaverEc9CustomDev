package com.customization.api.pdf2word.web;
import com.alibaba.fastjson.JSONObject;
import com.customization.api.pdf2word.service.PDF2WordService;
import com.customization.api.pdf2word.service.impl.PDF2WordServiceImpl;
import com.engine.common.util.ParamUtil;
import com.engine.common.util.ServiceUtil;
import weaver.hrm.HrmUserVarify;
import weaver.hrm.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;


public class PDF2WordAction {

    private PDF2WordService getService(User user) {
        return ServiceUtil.getService(PDF2WordServiceImpl.class, user);
    }

    @GET
    @Path("/checkstatus")
    @Produces({MediaType.TEXT_PLAIN})
    public String getWsdlInfo2(@Context HttpServletRequest request, @Context HttpServletResponse response){
        String result="";
        try {
            //获取当前用户
            User user = HrmUserVarify.getUser(request, response);
            result=JSONObject.toJSONString(getService(user).checkWfPdfStatus(request, response));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @GET
    @Path("/collect")
    @Produces({MediaType.TEXT_PLAIN})
    public String collectWords(@Context HttpServletRequest request, @Context HttpServletResponse response){
        String result="";
        try {
            //获取当前用户
            User user = HrmUserVarify.getUser(request, response);
            result= JSONObject.toJSONString(getService(user).collectWords(request, response));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

}
