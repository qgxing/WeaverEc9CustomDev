package com.customization.api.pdf2word.cmd;


import com.customization.commons.Console;
import com.engine.common.biz.AbstractCommonCommand;
import com.engine.common.entity.BizLogContext;
import com.engine.common.util.ParamUtil;
import com.engine.core.interceptor.CommandContext;
import net.sf.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.hrm.HrmUserVarify;
import weaver.hrm.User;
import weaver.soa.workflow.request.RequestInfo;
import weaver.soa.workflow.request.RequestService;
import weaver.workflow.workflow.WorkflowComInfo;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class CheckWfPdfStatusCmd extends AbstractCommonCommand<Map<String,Object>> {
    private HttpServletRequest request=null;
    private HttpServletResponse response=null;
    public CheckWfPdfStatusCmd(HttpServletRequest request, HttpServletResponse response){
        this.request=request;
        this.response=response;
        Console.log("ReadDocsPictureByOrgCmd=request:" + request + ";response:" + response);
        this.user= HrmUserVarify.getUser(request, response);
        this.params= ParamUtil.request2Map(request);

    }
    @Override
    public BizLogContext getLogContext() {
        return null;
    }

    @Override
    public Map<String, Object> execute(CommandContext commandContext) {

        Console.log("params:"+params);
        Console.log("user:"+user.getLastname()+";"+user.getUID());

        JSONObject result= new JSONObject();

        JSONObject apidesc= new JSONObject();
        JSONObject resultDesc= new JSONObject();
        JSONObject paramsDesc= new JSONObject();
        Map<String, Object> apidatas = new HashMap<String, Object>();
        result.put("status","-1");
        resultDesc.put("status","-1:未传递wfid参数; 0:没有设置附件字段; 1:正常返回,增加提取PDF的业务逻辑");
        resultDesc.put("fieldid","返回的附件字段ID");

        String wfid= Util.null2String(params.get("wfid"));
        paramsDesc.put("wfid","传递当前流程的流程id");
        if(!wfid.isEmpty()){
            RecordSet rs= new RecordSet();
            WorkflowComInfo workflowComInfo=new WorkflowComInfo();
            Console.log("当前流程wfid:"+wfid);
            String Activeversionid= Util.null2String(workflowComInfo.getActiveversionid(wfid));
            Console.log("当前流程活动版本wfid:"+Activeversionid);
            if(!Activeversionid.isEmpty()){
                wfid=Activeversionid;
            }
            Console.log("最终流程wfid:"+wfid);


            rs.execute("select * from uf_pdf2txt where wfid='"+wfid+"'");
            if(rs.first()){
                result.put("status","1");
                result.put("fieldid",rs.getString("fjsjkmc"));
            }else{
                result.put("status","0");
            }
        }
        apidatas.put("apidata",result);

        apidesc.put("params",paramsDesc);
        apidesc.put("result",resultDesc);
        apidatas.put("apiDataDesc",apidesc);

        return apidatas;
    }
}
