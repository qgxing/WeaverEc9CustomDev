package com.customization.proxy.impl;


import com.weaverboot.frame.ioc.anno.classAnno.WeaIocReplaceComponent;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaReplaceAfter;
import com.weaverboot.frame.ioc.handler.replace.weaReplaceParam.impl.WeaAfterReplaceParam;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@WeaIocReplaceComponent("ResourceBrowserService")
public class HrmBrowserServiceImpl {


    //人力资源字段返回值展现调整
    @WeaReplaceAfter(value = "/api/public/browser/complete/1",order = 1)
    public String after(WeaAfterReplaceParam weaAfterReplaceParam){
        String data = weaAfterReplaceParam.getData();//这个就是接口执行完的报文

        JSONObject json = JSONObject.fromObject(data);
        if(json.containsKey("datas")){
            JSONArray jsonArray=json.getJSONArray("datas");
            for (int i = 0; i <jsonArray.size() ; i++) {
                json.getJSONArray("datas").getJSONObject(i).put("lastname",
                        json.getJSONArray("datas").getJSONObject(i).get("title"));
            }
        }
        return json.toString();
    }

    //人力资源字段返回值展现调整
    @WeaReplaceAfter(value = "/api/public/browser/complete/17",order = 1)
    public String afterMultipleResource(WeaAfterReplaceParam weaAfterReplaceParam){
        String data = weaAfterReplaceParam.getData();//这个就是接口执行完的报文
        data="{\"datas\":[{\"subcompanyid1span\":\"股份公司\",\"subcompanyid\":\"21\",\"pinyinlastname\":\"张雅婷pinyinlastname\",\"departmentidspan\":\"信息中心\",\"departmentid\":\"28\",\"icon\":\"/messager/usericon/loginid20161027135106.jpg\",\"subcompanyname\":\"股份公司\",\"type\":\"resource\",\"supsubcompanyname\":\"\",\"title\":\"张雅婷|JAVA工程师|股份公司|信息中心\",\"lastname\":\"张雅婷|JAVA工程师|股份公司|信息中心\",\"jobtitlespan\":\"JAVA工程师\",\"jobtitlename\":\"JAVA工程师\",\"departmentname\":\"信息中心\",\"lastnamespan\":\"张雅婷lastnamespan\",\"name\":\"张雅婷name\",\"id\":\"5973\",\"displayKeys\":[\"lastnamespan\",\"jobtitlespan\"],\"nodeid\":\"resource_5973x\",\"supsubcompanyid\":\"0\"}]}\n";
        JSONObject json = JSONObject.fromObject(data);
        if(json.containsKey("datas")){
            JSONArray jsonArray=json.getJSONArray("datas");
            for (int i = 0; i <jsonArray.size() ; i++) {
                json.getJSONArray("datas").getJSONObject(i).put("lastname",
                        json.getJSONArray("datas").getJSONObject(i).get("title"));
            }
        }


        return json.toString();
    }


}
