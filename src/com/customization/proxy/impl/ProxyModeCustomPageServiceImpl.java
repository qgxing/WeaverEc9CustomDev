package com.customization.proxy.impl;


import com.weaverboot.frame.ioc.anno.classAnno.WeaIocReplaceComponent;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaReplaceAfter;
import com.weaverboot.frame.ioc.handler.replace.weaReplaceParam.impl.WeaAfterReplaceParam;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import weaver.hrm.HrmUserVarify;
import weaver.hrm.User;

import java.util.Base64;
import java.util.Map;

@WeaIocReplaceComponent("ModeCustomPageServiceImpl")
public class ProxyModeCustomPageServiceImpl {




    @WeaReplaceAfter(value = "/api/cube/custompage/getCustomPageViewData", order = 1, description = "接口请求后处理自定义页面加密参数")
    public String afterGetCustomPageViewData(WeaAfterReplaceParam weaAfterReplaceParam){

        JSONObject json = JSONObject.fromObject(weaAfterReplaceParam.getData());

        //User user = HrmUserVarify.getUser(weaAfterReplaceParam.getRequest(),weaAfterReplaceParam.getResponse());

        Map params=weaAfterReplaceParam.getParamMap();

        String id = params.get("id").toString();//原来的ID
        try {
            String secret=params.get("secret").toString();
            byte[] bytes = id.getBytes();
            //Base64 加密
            String encoded = Base64.getEncoder().encodeToString(bytes);
            if(!secret.equals(encoded)){//原ID和secret一致时赋值原ID
                json.put("detail",new JSONArray[]{});
                json.put("windowTitle","非法访问!!!");
                json.put("Customname","非法访问!!!");
            }
        }catch (Exception e){
            e.printStackTrace();
            json.put("detail",new JSONArray[]{});
            json.put("windowTitle","非法访问!!!");
            json.put("Customname","非法访问!!!");

        }

        json.put("Proxy_desc","页面加密跳转，代理类ProxyModeCustomPageServiceImpl");
        return json.toString();

    }
}
