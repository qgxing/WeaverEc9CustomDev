package com.customization.proxy;

import com.customcode.webservice.crm.NoticeShareUtil;
import com.customization.commons.Console;
import com.engine.core.cfg.annotation.CommandDynamicProxy;
import com.engine.core.interceptor.AbstractCommandProxy;
import com.engine.core.interceptor.Command;

import com.engine.crm.cmd.customer.CustomerAddCmd;
import com.engine.cube.cmd.resource.ResourceViewCmd;
import net.sf.json.JSONArray;

import java.util.Map;

@CommandDynamicProxy(target = CustomerAddCmd.class, desc="重写客户新建的方法 CustomerAddCmd.cmd")
public class CustomCustomerAddCmd  extends AbstractCommandProxy<Map<String,Object>> {
    @Override
    public Map<String, Object> execute(Command<Map<String, Object>> targetCommand) {
        Console.log("执行CustomerAddCmd的方法");

        //获取到被代理对象
        CustomerAddCmd rmCmd = (CustomerAddCmd) targetCommand;
        //获取被代理对象的参数
        Map<String, Object> params = rmCmd.getParams();
        Console.log(params.toString());


        Map<String, Object> result = nextExecute(targetCommand);
        if(null!=result.get("datas")) {
            String customerid = result.get("datas").toString();
            NoticeShareUtil noticeShareUtil= new NoticeShareUtil();

        }

        //对返回值做加工处理
        result.put("我是Test的key", "我是");
        result.put("params", params);




        return result;
    }

}