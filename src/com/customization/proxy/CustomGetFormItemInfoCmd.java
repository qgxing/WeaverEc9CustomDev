package com.customization.proxy;

import com.customization.commons.Console;
import com.engine.core.cfg.annotation.CommandDynamicProxy;
import com.engine.core.interceptor.AbstractCommandProxy;
import com.engine.core.interceptor.Command;
import com.engine.crm.cmd.customer.GetFormItemInfoCmd;
import com.engine.cube.cmd.resource.ResourceViewCmd;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import weaver.conn.RecordSet;

import java.util.Map;

/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName CustomGetFormItemInfoCmd.java
 * @Description TODO
 * @createTime 2020-05-29 11:17:00
 */
@CommandDynamicProxy(target = GetFormItemInfoCmd.class, desc="重写客户信息页面 GetFormItemInfoCmd.cmd")
public class CustomGetFormItemInfoCmd extends AbstractCommandProxy<Map<String,Object>> {

    @Override
    public Map<String, Object> execute(Command<Map<String, Object>> targetCommand) {
        System.out.println(getClass().getName() + "command 执行之前做一些事");

        //获取到被代理对象
        GetFormItemInfoCmd rmCmd = (GetFormItemInfoCmd) targetCommand;
        //获取被代理对象的参数
        Map<String, Object> params = rmCmd.getParams();

        Console.log(params.toString());

        //对参数做预处理
        //TODO
        //参数回写
        //rmCmd.setParams(params);
        //执行标准的业务处理
        Map<String, Object> result = nextExecute(targetCommand);


        //对返回值做加工处理 Start
        result.put("CommandDynamicProxy", "CustomGetFormItemInfoCmd");
        result.put("params", params);
        if(result.containsKey("datas")){


            RecordSet rs=new RecordSet();
            String customerid =(String) params.get("customerId");
            String crmcode2="";
            try{
                rs.execute("select crmcode from Crm_CustomerInfo where id='"+customerid+"'");
                String crmcode="";
                rs.first();
                crmcode=rs.getString("crmcode");
                if(!crmcode.equals("")){
                    rs.execute("select crmcode1,crmcode2 from uf_crmRelation where crmcode1='"+crmcode+"'");
                    while(rs.next()){
                        //添加查询到的数据生成json
                        crmcode2+=rs.getString("crmcode2")+",";
                    }
                    if (crmcode2.endsWith(",")) {
                        crmcode2 = crmcode2.substring(0, crmcode2.length() - 1);
                    }
                }
            }catch(Exception e){
                e.toString();
            }finally{

            }



            JSONArray datas=JSONArray.fromObject(result.get("datas"));
            System.out.println(datas.toString());
            String res="{" +
                    "\"title\": \"二级经销商\"," +
                    "\"defaultshow\": true," +
                    "\"items\": [{" +
                    "\"formItemType\": \"INPUT\"," +
                    "\"labelcol\": 6," +
                    "\"colSpan\": 2," +
                    "\"fieldcol\": 17," +
                    "\"length\": 200," +
                    "\"viewAttr\": 3," +
                    "\"conditionType\": \"INPUT\"," +
                    "\"rules\": \"required|string\"," +
                    "\"label\": \"经销商编号\"," +
                    "\"otherParams\": {" +
                    "\"detailtype\": 1," +
                    "\"qfws\": 0," +
                    "\"format\": {}," +
                    "\"inputType\": \"\"," +
                    "\"style\": {}" +
                    "}," +
                    "\"value\": \""+crmcode2+"\"," +
                    "\"domkey\": [\"angel_jxs_lth\"]" +
                    "}]" +
                    "}";
            JSONObject object=JSONObject.fromObject(res);
            datas.add(object);

            result.put("datas", datas);
        }
        //对返回值做加工处理 End


        System.out.println(getClass().getName() + "command 执行之后做一些事");

        return result;
    }
}
